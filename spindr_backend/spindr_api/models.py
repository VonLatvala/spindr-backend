# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class User(models.Model):
    id = models.IntegerField(blank=True, null=False, primary_key=True)
    tinder_id = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    birth_date_timestamp = models.IntegerField(blank=True, null=True)
    distance_mi = models.FloatField(blank=True, null=True)
    discovery_long = models.FloatField(blank=True, null=True)
    discovery_lat = models.FloatField(blank=True, null=True)
    bio = models.TextField(blank=True, null=True)
    instagram_username = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user'


class Education(models.Model):
    id = models.IntegerField(blank=True, null=False, primary_key=True)
    user = models.ForeignKey('User', models.CASCADE, blank=True, null=True,
                             related_name='educations')
    tinder_school_id = models.IntegerField(blank=True, null=True)
    institution = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'education'


class InstagramPhoto(models.Model):
    id = models.IntegerField(blank=True, null=False, primary_key=True)
    user = models.ForeignKey('User', models.CASCADE, blank=True, null=True,
                             related_name="instagram_photos")
    url = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'instagram_photo'


class Photo(models.Model):
    id = models.IntegerField(blank=True, null=False, primary_key=True)
    user = models.ForeignKey('User', models.CASCADE, blank=True, null=True,
                             related_name="photos")
    tinder_photo_id = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    local_path = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'photo'


class Vote(models.Model):
    id = models.IntegerField(blank=True, null=False, primary_key=True)
    user = models.ForeignKey(User, models.CASCADE, blank=True, null=True,
                             related_name="votes")
    timestamp = models.IntegerField(blank=True, null=True)
    value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vote'


class Work(models.Model):
    id = models.IntegerField(blank=True, null=False, primary_key=True)
    user = models.ForeignKey(User, models.CASCADE, blank=True, null=True,
                             related_name="jobs")
    company = models.TextField(blank=True, null=True)
    position = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'work'
