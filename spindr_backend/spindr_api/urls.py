from django.conf.urls import url
from spindr_api import views

urlpatterns = [
    url(r'^user_list/$', views.user_list),
    url(r'^user/(?P<pk>[0-9]+)/$', views.user_detail),
]
