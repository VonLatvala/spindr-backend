from django.apps import AppConfig


class SpindrApiConfig(AppConfig):
    name = 'spindr_api'
