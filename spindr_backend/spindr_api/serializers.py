from rest_framework import serializers
from spindr_api.models import User, Education, InstagramPhoto, Photo, Vote, Work


class EducationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Education
        fields = ('id', 'tinder_school_id', 'institution')

class InstagramPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = InstagramPhoto
        fields = ('id', 'url')

class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ('id', 'tinder_photo_id', 'url', 'local_path')

class VoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = ('id', 'timestamp', 'value')

class WorkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Work
        fields = ('id', 'company', 'position')

class UserSerializer(serializers.ModelSerializer):
    photos = PhotoSerializer(many=True)
    educations = EducationSerializer(many=True)
    instagram_photos = InstagramPhotoSerializer(many=True)
    votes = VoteSerializer(many=True)
    jobs = WorkSerializer(many=True)
    class Meta:
        model = User
        fields = ('id', 'tinder_id', 'name', 'birth_date_timestamp',
                  'distance_mi', 'discovery_long', 'discovery_lat',
                  'bio', 'instagram_username', 'photos', 'instagram_photos',
                  'jobs', 'educations', 'votes')

class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','tinder_id')



"""
class SnippetSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(required=False, allow_blank=True, max_length=100)
    code = serializers.CharField(style={'base_template': 'textarea.html'})
    linenos = serializers.BooleanField(required=False)
    language = serializers.ChoiceField(choices=LANGUAGE_CHOICES, default='python')
    style = serializers.ChoiceField(choices=STYLE_CHOICES, default='friendly')

    def create(self, validated_data):
        Create and return a new `Snippet` instance, given the validated data.
        return Snippet.objects.create(**validated_data)

    def update(self, instance, validated_data):
        Update and return an existing `Snippet` instance, given the validated data.
        instance.title = validated_data.get('title', instance.title)
        instance.code = validated_data.get('code', instance.code)
        instance.linenos = validated_data.get('linenos', instance.linenos)
        instance.language = validated_data.get('language', instance.language)
        instance.style = validated_data.get('style', instance.style)
        instance.save()
        return instance

class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    tinder_id = models.CharField()
    name = models.CharField()
    birth_date_timestamp = models.IntegerField()
    distance_mi = models.FloatField()
    discovery_long = models.FloatField()
    discovery_lat = models.FloatField()
    bio = models.CharField()
    instagram_username = models.CharField()

class User(models.Model):
    id = models.IntegerField(null=False, primary_key=True)
    tinder_id = models.CharField()
    name = models.CharField()
    birth_date_timestamp = models.IntegerField()
    distance_mi = models.FloatField()
    discovery_long = models.FloatField()
    discovery_lat = models.FloatField()
    bio = models.CharField()
    instagram_username = models.CharField()


class Education(models.Model):
    id = models.IntegerField(null=False, primary_key=True)
    user = models.ForeignKey('User', models.CASCADE,  )
    tinder_school_id = models.IntegerField()
    institution = models.CharField()


class InstagramPhoto(models.Model):
    id = models.IntegerField( null=False, primary_key=True)
    user = models.ForeignKey('User', models.CASCADE,  )
    url = models.CharField()


class Photo(models.Model):
    id = models.IntegerField(null=False, primary_key=True)
    user = models.ForeignKey('User', models.CASCADE,  )
    tinder_photo_id = models.CharField()
    url = models.CharField()
    local_path = models.CharField()


class Vote(models.Model):
    id = models.IntegerField(null=False, primary_key=True)
    user = models.ForeignKey(User, models.CASCADE,  )
    timestamp = models.IntegerField()
    value = models.CharField()


class Work(models.Model):
    id = models.IntegerField(null=False, primary_key=True)
    user = models.ForeignKey(User, models.CASCADE,  )
    company = models.CharField()
    position = models.CharField()
    """
